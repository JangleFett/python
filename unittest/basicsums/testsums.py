#!/usr/bin/env python3

import unittest
import sums

class test_sums(unittest.TestCase):

  def test_add(self):
    x=sums.sums()
    print("Running add")
    assert x.add(3) == 3, "The add method failed to add"

  def test_subtract(self):
    x=sums.sums()
    print("Running subtract")
    x.add(5)
    assert x.subtract(3) == 2, "The subtract failed"

if __name__ == "__main__":
  unittest.main()
