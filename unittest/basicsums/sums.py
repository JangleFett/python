#!/usr/bin/env python3

class sums:

  def __init__(self):
    self.result=0.0

  def add(self, a):
    self.result = self.result+a
    return self.result

  def subtract(self,a):
    self.result = self.result - a
    return self.result

  def showvalue(self):
    print(self.result)

if __name__ == "__main__":
  x = sums()
  print(x.add(5)) # Should print 5
  x.subtract(2) # Should set x's result to 3
  print(x.result) # Should print 3
  x.showvalue() # Should print 3
