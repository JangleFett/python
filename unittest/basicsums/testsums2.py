#!/usr/bin/env python3

import unittest
import sums

class test_sums(unittest.TestCase):
  x = sums.sums()

  def test_add(self):
    print("Running add")
    assert self.x.add(5) == 5, "The add method failed to add"

  def test_subtract(self):
    print("Running subtract")
    assert self.x.subtract(3) == 2, "The subtract failed"

if __name__ == "__main__":
  unittest.main()
