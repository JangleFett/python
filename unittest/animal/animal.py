#!/usr/bin/env python3

class Animal:

    def __init__(self,legs,colour,fs):
        self.legs=legs
        self.weight=0.0
        self.height=0.1
        self.age=0
        self.colour=colour
        self.furOrSkin=fs

    def birthday(self):
        self.age=self.age+1
        return self.age

    def eat(self):
        self.weight=self.weight+0.5
        self.height=self.height+0.5

    def run(self):
        self.weight=self.weight-0.1

    def showweight(self):
        return self.weight

    def showheight(self):
        return self.height

if __name__ == "__main__":
    fox=Animal(4,"Brown","Fur")
    fox.eat()
    print(fox.showweight())
    print(fox.showheight())
