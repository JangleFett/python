#!/usr/bin/env python3

import unittest
import animal

class Test(unittest.TestCase):
    fox = animal.Animal("4","Brown","Fur")
    cat = animal.Animal("4","Grey","Skin")

    def test_0_fox(self):
        # Fox eats
        self.fox.eat()
        assert self.fox.showweight() == 0.5, "Fox failed to gain weight" # Fox eat once

    def test_1_fox(self):
        # Fox height
        assert self.fox.showheight() == 0.6, "Fox is not growing" # Fox eat once

    def test_2_fox(self):
        # Show fox eats 2 more times
        self.fox.eat()
        self.fox.eat()
        assert self.fox.showweight() == 1.5, "Fox did not gain weight"

    def test_3_fox(self):
        # Fox height
        assert self.fox.showheight() == 1.6, "Fox is not growing" # Fox eat once

    def test_4_fox(self):
        # Show fox running
        self.fox.run()
        assert self.fox.showweight() == 1.4, "Fox didn't lose weight"

if __name__ == "__main__":
    unittest.main()
