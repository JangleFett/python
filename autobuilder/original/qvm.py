#!/usr/bin/env python3

import boto3
import sys

def queryvm(id):
    ec2=boto3.client('ec2', region_name="eu-west-1")

    ec2data=ec2.describe_instances(
        InstanceIds = [id]
    )

    print(ec2data['Reservations'][0]['Instances'][0]['PublicIpAddress'])
    return(ec2data)

if __name__ == "__main__":
    queryvm(sys.argv[1])
