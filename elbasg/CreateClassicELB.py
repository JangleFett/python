#!/usr/bin/env python3

import boto3

elbclient = boto3.client('elb','eu-west-1')
lbname="StevesELB"

# Create ELB
elb=elbclient.create_load_balancer(
        LoadBalancerName=lbname,
        Listeners=[
        {
            'Protocol': 'HTTP',
            'LoadBalancerPort': 80,
            'InstanceProtocol': 'HTTP',
            'InstancePort': 80
        }
        ],
        Subnets=['subnet-a94474e1','subnet-953a58cf'],
        Tags=[
            {
                'Key': 'Name',
                'Value': lbname
            }
        ]
)
