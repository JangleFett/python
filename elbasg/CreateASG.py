#!/usr/bin/env python3

import boto3
import sys

asgclient = boto3.client('autoscaling','eu-west-1')

# Create ASG

lbname="StevesELB"

# Launch Configuration
lc=asgclient.create_launch_configuration(
    LaunchConfigurationName="StevesLC",
    ImageId="ami-06ce3edf0cff21f07",
    InstanceType='t2.micro',
    KeyName="steveshillingacademyie",
    UserData='yum -y install httpd; service httpd start',
    AssociatePublicIpAddress=True
)

asg=asgclient.create_auto_scaling_group(
        AutoScalingGroupName='StevesASG',
        LaunchConfigurationName='StevesLC',
        MinSize=1,
        MaxSize=3,
        DesiredCapacity=1,
        LoadBalancerNames=[lbname],
        AvailabilityZones=['eu-west-1a','eu-west-1b','eu-west-1c']
    )

attach=asgclient.attach_load_balancers(
    AutoScalingGroupName='StevesASG',
    LoadBalancerNames=[lbname]
)
